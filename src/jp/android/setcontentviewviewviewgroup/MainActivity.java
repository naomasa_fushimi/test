package jp.android.setcontentviewviewviewgroup;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.ArrayList;

import android.support.v7.app.ActionBarActivity;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.ViewGroup.LayoutParams;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.FrameLayout;


public class MainActivity extends ActionBarActivity implements OnTouchListener,OnClickListener{

	private LayoutParams viewlayout = new LayoutParams(LayoutParams.WRAP_CONTENT,LayoutParams.WRAP_CONTENT);
	private LayoutParams viewlayout2 = new LayoutParams(LayoutParams.WRAP_CONTENT,60);
	BoxView view;
	CircleView view2;
	Button button,button2,button3,button4,button5,button6,button7;
	FrameLayout framelayout,frameview;
	LinearLayout linearlayout,linearlayout2,linearlayout3;
	private boolean viewset=true;
	Bitmap bitmap0,bitmap1;
	ArrayList<Integer> list = new ArrayList<Integer>();
	String FILE_NAME = "test.txt";
	
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		framelayout  =new FrameLayout(this);
		frameview  =new FrameLayout(this);
		linearlayout = new LinearLayout(this);
		linearlayout2 = new LinearLayout(this);
		linearlayout3 = new LinearLayout(this);
		
		linearlayout2.setOrientation(LinearLayout.VERTICAL);
		
		view = new BoxView(this);
		view.setOnTouchListener(this);
		
		button = new Button(this);
		button.setText("新規");
		button.setId(-2);
		button.setOnClickListener(this);
		
		button2 = new Button(this);
		button2.setText("上書き保存");
		button2.setId(-3);
		button2.setOnClickListener(this);
		
		button3 = new Button(this);
		button3.setText("開く");
		button3.setId(-4);
		button3.setOnClickListener(this);
		
		button4 = new Button(this);
		button4.setText("空にする");
		button4.setId(-5);
		button4.setOnClickListener(this);
		
		button5 = new Button(this);
		button5.setText("ひとつ消す");
		button5.setId(-6);
		button5.setOnClickListener(this);

		button6 = new Button(this);
		button6.setText("名前を付けて保存");
		button6.setId(-7);
		button6.setOnClickListener(this);

		button7 = new Button(this);
		button7.setText("o");
		button7.setId(-8);
		button7.setOnClickListener(this);
		
		linearlayout.addView(button3,viewlayout2);
		linearlayout.addView(button2,viewlayout2);
		linearlayout.addView(button4,viewlayout2);
		linearlayout3.addView(button,viewlayout2);
		linearlayout3.addView(button5,viewlayout2);
		linearlayout3.addView(button6,viewlayout2);
		linearlayout.addView(button7,viewlayout2);
		
		linearlayout2.addView(linearlayout);
		linearlayout2.addView(linearlayout3);
		linearlayout2.addView(frameview);
		
		frameview.addView(view);
		framelayout.addView(linearlayout2);
		setContentView(framelayout);
		
	}

	@Override
	protected void onStart(){
		super.onStart();
		Log.d("onStart","called");
	}
	@Override
	protected void onResume(){
		super.onResume();
		Log.d("onResume","called");
	}

	private boolean mgetid = true;
	int j =0;
	@Override
	public boolean onTouch(View v, MotionEvent event) {
		int x = (int)event.getX();
        int y = (int)event.getY();    
        frameview.getChildCount();
        Log.d("child",String.valueOf(frameview.getChildCount()));
		if(mgetid){
			double a1 = (Math.random()*255);
			double b1 = (Math.random()*255);
			double c1 = (Math.random()*255);
			int a =(int)a1,b=(int)b1,c=(int)c1;
			view2 = new CircleView(this);
			view2.setXY(x, y,a,b,c);
			list.add(x);
			list.add(y);
			list.add(a);
			list.add(b);
			list.add(c);
			switch(event.getAction()){
			case MotionEvent.ACTION_DOWN:
				Log.d("Down","called");
				frameview.addView(view2,viewlayout);
				break;
			default:
				break;
		
			}
		}
		mgetid = true;
		
		Log.d("id-1","called");
		return false;
	}
	int k;
	@Override
	public void onClick(View v) {
		int i= frameview.getChildCount();
		int j= frameview.getChildCount();	
		switch(v.getId()){
		case -2:
			Log.d("id-2","called");
			list.clear();
			frameview.removeAllViews();
			frameview.addView(view);
			mgetid = false;
			break;
		case -3:
			Log.d("id-3","called");
			try{
				deleteFile(FILE_NAME);
				FileOutputStream out = openFileOutput(FILE_NAME, MODE_APPEND);
				BufferedWriter outFile = new BufferedWriter(new OutputStreamWriter(out));
				k = list.size()-1;
				Log.d("i",String.valueOf(i));
				for(int l = 0; l <= k; l++){
					outFile.write(String.valueOf(list.get(l))+"\r\n");
					Log.d("list.l",String.valueOf(list.get(l)));
				}
				outFile.close();
				Log.d("Bitmap","called");
			}
			catch(FileNotFoundException e) {
				Log.d("FNF","called");
		    }
			catch(IOException e){
				Log.d("IO","called");
			}
			
			mgetid = false;
			break;
		case -4:
			frameview.removeAllViews();
			frameview.addView(view);
			try{
				FileInputStream in = openFileInput(FILE_NAME);
				BufferedReader inFile = new BufferedReader(new InputStreamReader(in));
				String line;
				
				while ((line = inFile.readLine()) != null) {
					view2 = new CircleView(this);
					Log.d("line",line);
					int x = Integer.parseInt(line);
					line = inFile.readLine();
					int y = Integer.valueOf(line).intValue();
					line = inFile.readLine();
					int a = Integer.valueOf(line).intValue();
					line = inFile.readLine();
					int b = Integer.valueOf(line).intValue();
					line = inFile.readLine();
					int c = Integer.valueOf(line).intValue();
					view2.setXY(x, y,a,b,c);
					list.add(x);
					list.add(y);
					list.add(a);
					list.add(b);
					list.add(c);
					frameview.addView(view2,viewlayout);
				}
			}catch(IOException e){
				
			}
			Log.d("id-4","called");
			break;
		case -5:
			deleteFile(FILE_NAME);
			deleteFile("main_filename_file.txt");
			break;
		case -6:
			if(frameview.getChildCount()>1){
				frameview.removeViewAt(frameview.getChildCount()-1);
				for(int m = 0 ; m < 5 ; m++){
					list.remove(list.size()-1);
					Log.d("m","called");
				}
			}
			break;
		case -7:
			TouchDialog dialog = new TouchDialog();
			dialog.hikisuu(true, this);
			dialog.show(getFragmentManager(), "");
			break;
		case -8:
			TouchDialog dialog2 = new TouchDialog();
			dialog2.hikisuu(false,this);
			dialog2.show(getFragmentManager(), "");
			Log.d("dialog2","called");
			break;
		default:
			break;
		}
		mgetid = true;
	}

	public void ReturnDialog(String x) {
		Filesave filesave = new Filesave(this,x);
		int i= frameview.getChildCount();
		try{
			deleteFile(x);
			FileOutputStream out = openFileOutput(x, MODE_APPEND);
			BufferedWriter outFile = new BufferedWriter(new OutputStreamWriter(out));
			k = list.size()-1;
			Log.d("i",String.valueOf(i));
			for(int l = 0; l <= k; l++){
				outFile.write(String.valueOf(list.get(l))+"\r\n");
				Log.d("list.l",String.valueOf(list.get(l)));
			}
			outFile.close();         	
            Log.d("ReturnDialog","called");
		}
		catch(FileNotFoundException e) {
			Log.d("FNF","called");
	    }
		catch(IOException e){
			Log.d("IO","called");
		}
		deleteFile(x);
		mgetid = false;
	}

	public void Return2(String string) {
		frameview.removeAllViews();
		frameview.addView(view);
		try{
			FileInputStream in = openFileInput(string);
			BufferedReader inFile = new BufferedReader(new InputStreamReader(in));
			String line;
			
			while ((line = inFile.readLine()) != null) {
				view2 = new CircleView(this);
				Log.d("line",line);
				int x = Integer.parseInt(line);
				line = inFile.readLine();
				int y = Integer.valueOf(line).intValue();
				line = inFile.readLine();
				int a = Integer.valueOf(line).intValue();
				line = inFile.readLine();
				int b = Integer.valueOf(line).intValue();
				line = inFile.readLine();
				int c = Integer.valueOf(line).intValue();
				view2.setXY(x, y,a,b,c);
				list.add(x);
				list.add(y);
				list.add(a);
				list.add(b);
				list.add(c);
				frameview.addView(view2,viewlayout);
			}
		}catch(IOException e){	
		}
		deleteFile(string);
	}
}
