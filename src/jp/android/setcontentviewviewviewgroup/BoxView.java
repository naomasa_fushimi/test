package jp.android.setcontentviewviewviewgroup;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class BoxView extends View {
    Paint mfillpen;
    
	private int w,h;
	public BoxView (Context context){
		super(context);
		init();
	}

	public void init(){
		mfillpen = new Paint();
		mfillpen.setAntiAlias(true);
		double a1 = (Math.random()*255);
		double b1 = (Math.random()*255);
		double c1 = (Math.random()*255);
		int a =(int)a1,b=(int)b1,c=(int)c1;
		mfillpen.setColor(Color.WHITE);
	}
	
	/*public void setWidth(){
		getWidth();
	}
	public void setHeight(){
		getHeight();
	}
	*/
/*
	Bitmap bitmap;
	private boolean bitmapdraw = false;
	
	public void setbitmap(boolean v,Bitmap b){
		bitmapdraw = v;
		bitmap = b;
	}
*/	
	@Override
	protected void onDraw(Canvas canvas) {
/*		if(bitmapdraw){
			Log.d("bitmapdraw1","called");
			canvas.drawBitmap(bitmap,0,0,null);
			Log.d("bitmapdraw2","called");
			bitmapdraw = false;
		}
		else{
*/	
		w = getWidth();
		h = getHeight();
		canvas.drawRect(0 , 0 , w, h, mfillpen);
		Log.d("onDraw","called");
	
//		}
	}
}
