package jp.android.setcontentviewviewviewgroup;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;

public class CircleView extends View {

	Paint mFillPen;
	
	
	int x,y;
	
	public CircleView(Context context) {
		super(context);
		init();
	}

	public CircleView(Context context, AttributeSet attrs) {
		super(context, attrs);
		init();
		// TODO 自動生成されたコンストラクター・スタブ
	}

	public CircleView(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		init();
		// TODO 自動生成されたコンストラクター・スタブ
	}
	private void init() {
		mFillPen = new Paint();
		mFillPen.setAntiAlias(false);
		}
	public void setXY(int v,int z,int a,int b,int c){
		x = v;
		y = z; 
		mFillPen.setColor(Color.rgb(a, b, c));
	}
	
	Bitmap bitmap;
	private boolean bitmapdraw = false;
	
	public void setbitmap(boolean v,Bitmap b){
		bitmapdraw = v;
		bitmap = b;
	}
	
	@Override
	protected void onDraw(Canvas canvas) {
		if(bitmapdraw){
			Log.d("bitmapdraw1","called");
			canvas.drawBitmap(bitmap,0,0,null);
			Log.d("bitmapdraw2","called");
			//bitmapdraw = false;
		}
		else{
		
			int w = getWidth();
			int h = getHeight();
			canvas.drawCircle( x, y, Math.min(w, h) / 30, mFillPen);
		}
	}
}