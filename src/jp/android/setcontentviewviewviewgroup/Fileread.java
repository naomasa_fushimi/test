package jp.android.setcontentviewviewviewgroup;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;

import android.app.Activity;
import android.content.Context;
import android.util.Log;
import android.widget.ArrayAdapter;

public class Fileread extends Activity{

	Context context;
	private static final String FILE_NAME = "main_filename_file.txt";
	ArrayList<String> list = new ArrayList<String>();
	
	
	public Fileread(Context c){
		context = c;
		Log.d("Fileread","called");
	}
	
	ArrayList<String> fileread(){
		Log.d("fileread","called");
		FileInputStream in;
		try {
			in = context.openFileInput(FILE_NAME);
			BufferedReader inFile = new BufferedReader(new InputStreamReader(in));
			String line;
			while ((line = inFile.readLine()) != null) {
				list.add(line);
			};
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			// TODO 自動生成された catch ブロック
			e.printStackTrace();
		}
		return list;
	}
}
