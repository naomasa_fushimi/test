package jp.android.setcontentviewviewviewgroup;


import java.util.ArrayList;

import android.widget.AdapterView.OnItemClickListener;
import android.R;
import android.content.Context;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup.LayoutParams;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;

public class TouchDialog extends DialogFragment {
	
	private LayoutParams viewlayout = new LayoutParams(300,60);
	private LayoutParams viewlayout2 = new LayoutParams(LayoutParams.MATCH_PARENT,60);
	private LayoutParams viewlayout3 = new LayoutParams(LayoutParams.MATCH_PARENT,LayoutParams.MATCH_PARENT);
	private String x = null;
	ArrayList<String> list = new ArrayList<String>();

//	LayoutInflater inflater = LayoutInflater.from(getActivity());
	LinearLayout linearlayout;
	Context c;
	
	private boolean inout;
	
	
	public TouchDialog(){
	}
	
	public void hikisuu(boolean io,Context context){
		inout = io;
		c = context;
	}
	
	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState) {
		AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
		final EditText edit = new EditText(getActivity());
		if(inout){
			linearlayout = new LinearLayout(getActivity());
			TextView text = new TextView(getActivity());
			text.setText(".txt");
			linearlayout.addView(edit,viewlayout);
			linearlayout.addView(text,viewlayout2);
			builder.setTitle("ファイル名入力");
			builder.setView(linearlayout);
			builder.setNegativeButton("cancel",null);
			builder.setPositiveButton("保存", new DialogInterface.OnClickListener(){
				public void onClick(DialogInterface dialog, int which) {
					x = edit.getText().toString();
					Log.d("x",x);
					MainActivity callingActivity = (MainActivity) getActivity();
					callingActivity.ReturnDialog(x+".txt");
					dismiss();
				}
			});
		}
		else{
			int p;
			linearlayout = new LinearLayout(getActivity());
			linearlayout.setOrientation(LinearLayout.VERTICAL);
			Fileread read = new Fileread(c);
			list = read.fileread();
			Log.d("readlist", String.valueOf(list.size()));
			builder.setTitle("ファイル名を選んでください");
			ArrayAdapter<String> array = new ArrayAdapter<String>(c,R.layout.simple_list_item_1,list);
			ListView lv = new ListView(c);
			lv.setAdapter(array);
			lv.setOnItemClickListener(new OnItemClickListener(){
				public void onItemClick(AdapterView<?> item, View view,
						int position, long id) {
					list.get(position);
					MainActivity calling = (MainActivity) getActivity();
					Log.d("position",list.get(position));
					calling.Return2(list.get(position));
					dismiss();
				}
				
			});
			builder.setView(lv);
			builder.setNegativeButton("cancel",null);
		}
		return builder.create();
	}
}